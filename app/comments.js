const express = require('express');
const router = express.Router();

const createRouter = (db) => {
    router.get('/', async (req, res) => {
        try {
            const comments = await db.getItems('comments');
            res.send(comments);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        const response = await db.getItem('comments', req.params.id);
        res.send(response[0]);
    });

    router.post('/', async (req, res) => {
        let comment = req.body;
        if (!comment.news_id) {
            return res.status(404).send({error: "Full all required fields!"})
        }
        const newComment = await db.createItem('comments', comment);
        res.send(newComment);
    });

    router.delete('/:id', async (req, res) => {
        try {
            const response = await db.deleteItem('comments', req.params.id);
            res.send(response[0]);
        } catch (e) {
            res.send("Deletion problem")
        }
    });

    return router;
};

module.exports = createRouter;