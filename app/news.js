const express = require('express');
const {nanoid} = require('nanoid');
const multer  = require('multer');
const config = require('../config');
const router = express.Router();
const path = require("path");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = (db) => {

    router.get('/', async (req, res) => {
        try {
            const news = await db.getItems('news');
            const arr = news.map(article => {
                return {
                    'id': article.id,
                    'news_title': article.news_title,
                    'news_image': article.news_image,
                    'news_date': article.news_date}
            });
            res.send(arr);
        }
        catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        try {
            const response = await db.getItem('news', req.params.id);
            res.send(response[0]);
        }
        catch (e) {
            res.status(500).send(e);
    }
    });

    router.post('/', upload.single('news_image'), async (req, res) => {
        const article = {...req.body};
        if (!article.news_title || !article.news_description) {
            return res.status(404).send({error: "Fields are required"})
        }
        if (req.file) {
            article.news_image = req.file.filename;
        }
        const newArticle = await db.createItem('news', article);
        res.send(newArticle);
    });

    router.delete('/:id', async (req, res) => {
        const response = await db.deleteItem('news', req.params.id);
        res.send(response[0]);
    });

      return router;
};

module.exports = createRouter;