const express = require("express");
const cors = require('cors');
const mysql = require("mysql");
const config = require("./config");
const comments = require("./app/comments");
const news = require("./app/news");
const db = require('./mysql')
const app = express();
const port = 8000;

const connection = mysql.createConnection(config.db);

app.use(cors());
app.use(express.static('public'));
app.use(express.json());

connection.connect((err) => {
    if (err) {
        throw err;
    }

    app.use('/comments', comments(db(connection)));
    app.use('/news', news(db(connection)));

    console.log('connected to mysql');
    app.listen(port, () => {
        console.log('Server started on port http://localhost:' + port);
    });
});


