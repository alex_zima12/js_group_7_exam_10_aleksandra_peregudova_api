CREATE DATABASE IF NOT EXISTS news;
Use news;

CREATE TABLE IF NOT EXISTS news (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  news_title VARCHAR(255) NOT NULL UNIQUE,
  news_description TEXT(3000) NOT NULL,
  news_image VARCHAR(40) DEFAULT NULL,
  news_date DATETIME DEFAULT CURRENT_TIMESTAMP  
);

CREATE TABLE IF NOT EXISTS comments (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  news_id INT NOT NULL,
  CONSTRAINT FK_news
  FOREIGN KEY (news_id) 
  REFERENCES news (id) 
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  author VARCHAR(100) DEFAULT "Anonymous",
  commentar TEXT(3000) NOT NULL
  );

  INSERT INTO news (news_title, news_description,news_date)
  VALUES ("Hot news", "Political turmoil has gripped the Central Asian country after a disputed parliamentary election.","2020-01-01 10:10:10"),
  ("Israel and Sudan leaders", "Israel and Sudan leaders talk after US removes Khartoum from terrorism list","2020-01-03 10:10:10"),
  ("Boris Johnson's reputation collapses", "The British PM used to be the Teflon man of British politics, brushing off scandals. Not any more.","2020-01-02 10:10:10") ;
   
  INSERT INTO comments (news_id, author, commentar)
  VALUES (1, "Pushkin A.S.", "like horror cinema"),
  (1, "Bad Boy", "when will it end????????????");